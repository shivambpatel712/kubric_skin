THIS IS MY LIBRARY FOR DETECTING AND CHANGING SKIN COLOR.

Some Important Software Requirements:
1. X11 (Xquartz on Mac)
2. Boost and Boost-Python
3. Dlib

For Mac:
https://www.learnopencv.com/install-dlib-on-macos/

For Ubuntu:
https://www.learnopencv.com/install-dlib-on-ubuntu/

For Windows:
https://www.learnopencv.com/install-opencv-3-and-dlib-on-windows-python-only/

Usage:
Pass the following command-line arguments to __init__.py in morphing/ folder.
python3 __init__.py <color/image> <path_to_image_to_be_changed> <color_in_RGB_like_[12,34,56]/path_to_other_image> <path_where_image_is_to_be_saved>